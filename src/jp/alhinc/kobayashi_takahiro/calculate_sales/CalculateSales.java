package jp.alhinc.kobayashi_takahiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		final String FILE_PATH = args[0];
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		if (!readBranchFile(FILE_PATH, branchNames, branchSales)) {
			return;
		}

		if (!readRcdFileAndCalcu(FILE_PATH, branchSales)) {
			return;
		}

		if (!outputFile(FILE_PATH, branchNames, branchSales)) {
			return;
		}

	}

	private static boolean readBranchFile(final String FILE_PATH, Map<String, String> branchNames,
			Map<String, Long> branchSales) {

		File file = new File(FILE_PATH, "branch.lst");

		if (!file.exists()) {
			System.out.println("支店定義ファイルが存在しません");
			return false;
		}

		BufferedReader br = null;

		try {
			FileReader fileReader = new FileReader(file);
			br = new BufferedReader(fileReader);
			String branchData = "";
			String[] fileContents;
			
			while ((branchData = br.readLine()) != null) {
				if (!branchData.matches("^[0-9]{3},[一-龠ぁ-ゞァ-ヶa-zA-Z]+支店$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				fileContents = branchData.split(",");
				branchNames.put(fileContents[0], fileContents[1]);
				branchSales.put(fileContents[0], 0L);
			}

			br.close();

		} catch (IOException e) {
			e.printStackTrace();
			return false;
			
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
		}

		return true;
	}

	private static boolean readRcdFileAndCalcu(final String FILE_PATH, Map<String, Long> branchSales) {

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.matches("^[0-9]{8}.rcd$");
			}
		};

		/* 
		 * リストの連番チェック
		 */

		File file = new File(FILE_PATH);
		List<String> files = Arrays.asList(file.list(filter));
		Collections.sort(files);
		int originNum = 0;
		int laterNum = 0;

		for (int j = 0; j < files.size() - 1; j++) {
			originNum = Integer.parseInt(files.get(j).substring(0, 8));
			laterNum = Integer.parseInt(files.get(j + 1).substring(0, 8));
			if ((laterNum - originNum) != 1) {
				System.out.println("レコードファイルが連番になっていません。");
				return false;
			}
		}

		/* 
		 * ファイルを読み込んで、トータルの金額を算出
		 */

		BufferedReader br = null;
		FileReader fr = null;

		try {
			String branchData = "";
			String branchKey = "";
			boolean branchExists = false;
			Long total = 0L;
			int lineCount = 0;

			for (int i = 0; i < files.size(); i++) {
				file = new File(FILE_PATH, files.get(i));
				fr = new FileReader(file);
				br = new BufferedReader(fr);
				while ((branchData = br.readLine()) != null) {

					/*
					 * 売り上げファイルが2行以上あった場合のエラー処理
					 * while文終了後に、lineCountを0にしているため
					 * 3行以上あると弾く。					  
					 */
					
					if (2 <= lineCount) {
						System.out.println(files.get(i) + "のフォーマットが不正です");
						br.close();
						fr.close();
						return false;
					}

					/*
					 *  while文の中で1番最初に行う処理
					 * ブランチファイルの1行目が支店コードと一致していたら、
					 * branchExistsをtrueにして、continueでwhileの処理を終わらせる。
					 */
					
					if (branchSales.containsKey(branchData) && !branchExists) {
						branchKey = branchData;
						branchExists = true;
						lineCount++;
						continue;
					}

					/*
					 * 	支店コード無かった時のエラー処理
					 * while2回目の処理で、branchExistsがtrueではなかった＝レコードファイルの先頭行が
					 * ブランチファイルに含まれいてない支店コードとし、弾く。
					 */
					
					if (branchExists) {
						if (!branchData.matches("^[0-9]+$")) {
							System.out.println("予期せぬエラーが発生しました");
							br.close();
							fr.close();
							return false;
						}
						branchExists = false;
						lineCount++;
						total = branchSales.get(branchKey) + Long.valueOf(branchData);
					} else {
						System.out.println(files.get(i) + "の支店コードが不正です");
						br.close();
						fr.close();
						return false;
					}
				}
				
				if (10_000_000_000L <= total) {
					System.out.println("支店コード" + branchKey + "の値が10桁を超えました。");
					br.close();
					fr.close();
					return false;
				}
				
				branchSales.replace(branchKey, total);
				lineCount = 0;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null || fr != null) {
				try {
					br.close();
					fr.close();
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		}
		return true;
	}

	private static boolean outputFile(final String FILE_PATH, Map<String, String> branchNames,
			Map<String, Long> branchSales) {

		FileWriter fw = null;
		PrintWriter pw = null;

		try {
			fw = new FileWriter(FILE_PATH + "\\" + "branch.out");
			pw = new PrintWriter(new BufferedWriter(fw));

			for (String key : branchNames.keySet()) {
				pw.println(key + "," + branchNames.get(key) + "," + branchSales.get(key));
			}

			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (fw != null || pw != null) {
				try {
					fw.close();
					pw.close();
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
		}
		
		return true;
	}
}